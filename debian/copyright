Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: puzzle-jigsaw
Upstream-Contact: https://bitbucket.org/admsasha/puzzle-jigsaw/issues/new
Source: https://bitbucket.org/admsasha/puzzle-jigsaw
Comment: Some files had to be excluded (see Files-Excluded field below)
 because they are licensed under "Pexels License", which is not a DFSG
 compatible license. The file pexels-snapwire-6992.jpg was excluded because
 uses a non-free color profile.
Files-Excluded: samples/pexels-fabian-wiktor-994605.jpg
                samples/pexels-kourosh-qaffari-1921168.jpg
                samples/pexels-photo-1108099.jpeg
                samples/pexels-photo-1319515.jpeg
                samples/pexels-photo-142497.jpeg
                samples/pexels-photo-3551498.jpeg
                samples/pexels-photo-3637174.jpeg
                samples/pexels-snapwire-6992.jpg

Files: *
Copyright: 2020 DanSoft <dik@inbox.ru>
License: GPL-3+

Files: samples/*
Copyright: 2016-2017 Pexels <www.pexels.com>
License: CC0-1.0
Comment: The copyright dates were obtained from the following Pexels websites
 for each image:
 - https://www.pexels.com/photo/animal-pet-fur-head-33537/
 - https://www.pexels.com/photo/angry-animal-big-carnivore-302304/

Files: debian/*
Copyright: 2020-2024 Fabio Augusto De Muzio Tobich <ftobich@debian.org>
License: GPL-3+

Files: debian/upstream/org.bitbucket.puzzle-jigsaw.metainfo.xml
Copyright: 2020-2024 Fabio Augusto De Muzio Tobich <ftobich@gmail.com>
License: MIT

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: CC0-1.0
 To the extent possible under law, the author(s) have dedicated all copyright
 and related and neighboring rights to this software to the public domain
 worldwide. This software is distributed without any warranty.
 .
 You should have received a copy of the CC0 Public Domain Dedication along
 with this software. If not, see
 <https://creativecommons.org/publicdomain/zero/1.0/>.
 .
 On Debian systems, the complete text of the CC0 Public Domain Dedication
 can be found in "/usr/share/common-licenses/CC0-1.0".

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
